package coding;

public enum EnumCheck {
	EDIT ("edit"),
	VIEW ("view"),
	NO_ACESS ("no-acess") ;
	
	private final String name;

	public String getName() {
		return name;
	}

	private EnumCheck(String name) {
		this.name = name;
	}
}
